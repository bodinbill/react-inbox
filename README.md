## Available Scripts

In the project (using node 8 for development, compatible with node 10), you can run:

### `npm install`

Installs all dependent npm packages.

### `npm start`


Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.