import React from "react";
import { size } from "lodash";
import { List, AutoSizer } from "react-virtualized";
import Item from "./component/inbox/Item";
import AppHeader from "./component/header/Header";
import withData from "./component/utils/withData";
import "./App.scss";

const rowRenderer = ({ index, key, data, style }) => (
  <Item key={key} item={data[index]} style={style} />
);

const ROW_HEIGHT = 73;

const App = ({ data }) => {
  return (
    <div className="inbox container">
      <AppHeader />
      <div className="row">
        <AutoSizer>
          {({ height, width }) => (
            <List
              className="items"
              width={width}
              height={ROW_HEIGHT * 8}
              rowCount={size(data)}
              rowHeight={ROW_HEIGHT}
              overscanRowCount={25}
              rowRenderer={props => rowRenderer({ ...props, data })}
            />
          )}
        </AutoSizer>
      </div>
    </div>
  );
};

export default withData(App);
