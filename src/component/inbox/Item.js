import React from "react";
import {
  FaRegEnvelope,
  FaRegFlag,
  FaThumbtack,
  FaRegTrashAlt
} from "react-icons/fa";
import Avatar from "react-avatar";
import { get, isEmpty } from "lodash";
import "./Item.scss";

const ICON_SIZE = 28;
const Item = ({ item, style }) => {
  if (isEmpty(item)) return null;
  return (
    <div className="item container-fluid" style={style}>
      <div className="buttons">
        <FaRegTrashAlt />
        <FaRegFlag />
        <FaRegEnvelope />
        <FaThumbtack />
      </div>

      <div className="col-12 d-inline-flex justify-content-between">
        <a href={`mailto:${get(item, "from.email")}`} className="icon">
          <Avatar
            className="rounded-circle overflow-hidden"
            name={get(item, "from.name")}
            textSizeRatio={2.5}
            size={ICON_SIZE}
            maxInitials={2}
          />
        </a>
        <div className="details-wrap">
          <div className="details">
            <div className="from line">
              {get(item, "from.name")}
            </div>
            <div className="subject line">{get(item, "subject")}</div>
            <span className="body line">{get(item, "body")}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Item;
