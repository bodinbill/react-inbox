import React from "react";
import { mount } from "enzyme";

import Item from "./Item";

const data = {
  from: {
    name: "Now TV",
    email: "nowtv@test.com"
  },
  subject: "Grab another Pass, you need to be watching this...",
  body: "Oscar winners Sir Anthony Hopkins and Ed Harris."
};

it("renders correctly", () => {
  const wrapper = mount(<Item item={data} />);
  expect(wrapper.find(".from").text()).toBe("Now TV");
  expect(wrapper.find(".subject").text()).toBe(
    "Grab another Pass, you need to be watching this..."
  );
  expect(wrapper.find(".body").text()).toBe(
    "Oscar winners Sir Anthony Hopkins and Ed Harris."
  );
});

it("renders other body", () => {
  const wrapper = mount(<Item item={{ ...data, body: "other body" }} />);
  expect(wrapper.find(".from").text()).toBe("Now TV");
  expect(wrapper.find(".subject").text()).toBe(
    "Grab another Pass, you need to be watching this..."
  );
  expect(wrapper.find(".body").text()).toBe("other body");
});
