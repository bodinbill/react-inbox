import React from "react";
import { compose, withState } from "recompose";

const initialize = () => {
  return require("./large-data.json");
};

const withData = WrapperComponent =>
  compose(withState("data", "setData", initialize))(props => (
    <WrapperComponent {...props} />
  ));

export default withData;
