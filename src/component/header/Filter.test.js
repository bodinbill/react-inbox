import React from "react";
import { mount } from "enzyme";

import Filter from "./Filter";

it("renders correctly", () => {
  jest.spyOn(window, "alert").mockImplementation(() => {});

  const wrapper = mount(<Filter />);
  expect(wrapper.find(".filter").text()).toBe("Filter");
  wrapper.find(".filter").simulate("click");
  expect(window.alert).toBeCalledWith("Coming soon.");
});
