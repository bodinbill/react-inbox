import React from "react";
import { mount } from "enzyme";

import Header from "./Header";

it("renders correctly", () => {
  const wrapper = mount(<Header />);
  expect(wrapper.find("h1")).toHaveLength(1);
  expect(wrapper.find("h1").text()).toBe("Inbox");
  expect(wrapper.find(".filter")).toHaveLength(1);
});
