import React from "react";
import { FaAngleDown } from "react-icons/fa";
import "./Filter.scss";

const Filter = ({ className }) => {
  return (
    <div
      className={`filter ${className}`}
      onClick={() => {
        alert("Coming soon.");
      }}
    >
      Filter
      <span>
        <FaAngleDown />
      </span>
    </div>
  );
};

export default Filter;
