import React from "react";
import Filter from "./Filter";
import "./Header.scss";

const Item = () => {
  return (
    <div className="header container-fluid">
      <div className="container d-flex">
        <h1 className="p2">Inbox</h1>
        <Filter className="p2 ml-auto" />
      </div>
    </div>
  );
};

export default Item;
